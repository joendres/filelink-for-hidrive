# Filelink for Hidrive

This Thunderbird (68+) Add On makes it easy to send large attachments: It uploads the files to Strato's Hidrive service and inserts download links into the message.

HiDrive is a trademark of Strato AG. I'm not in any way connected to that company. I can only hope they let me use their trademark in the Addon's name, because it wouldn't make any sense without it.